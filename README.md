# DofusGuessr

Un Geoguessr, mais pour la carte de Dofus


# Licence

Le projet est sous license GPL-3.0-or-later.


# Développement et choix techniques

Toute aide au developpement du site / de la récolte des copies d'écran est
fortement bienvenue. N'hésitez pas à me contacter pour participer.

Les choix architecturaux ne sont pas fixés, n'hésitez pas à venir en discuter.
Pour l'instant on part sur un PHP-slim-framework / twig / leaflet, mais tout
est questionnable.

La question de l'écriture du code en Français ou en Anglais est aussi à
discuter.
